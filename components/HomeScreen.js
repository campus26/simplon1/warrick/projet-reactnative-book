import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';


export function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.titleAcceuil}>Mon APP React Native!</Text>
      <Button style={styles.buttonAcceuil} title="Consulter" onPress={() => navigation.navigate('Biblio')}/>
      <StatusBar style="auto" />
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#eec',
      alignItems: 'center',
      justifyContent: 'center',
    },
  
    titleAcceuil: {
      color: 'red',
      fontSize: 30,
      paddingBottom: 100,
    },
  
    buttonAcceuil: {
      color: Colors.black,
      borderRadius: 20,
    },
  });